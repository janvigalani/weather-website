(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {

    });

})(jQuery, document, window);

// COMMUNICATING WITH forecast.js

const defaultCity = "Mumbai";
const cityForm = document.querySelector('form');
const daysName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const monthsName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

const updateUI = (data, city) => {
    const days = document.getElementsByClassName('day');
    const date = document.querySelector('.date');
    const location = document.querySelector('.location');
    const humidity = document.getElementById('humidity');
    const windSpeed = document.getElementById('wind-speed');
    const windDeg = document.getElementById('wind-degree');
    const temps = document.getElementsByClassName('temp');
    const icons = document.getElementsByClassName('weather-icon');
    const imagePath = "images/icons/";

    // Set the current location on UI
    location.innerHTML = city;
    
    // Set the humidity
    humidity.innerHTML = data.list[0].main.humidity + "%";

    // Set the wind speed
    // 1m/sec = 3.6km/hr so we need to multiply the data with 3.6 as API is providing m/sec
    windSpeed.innerHTML = Math.round((data.list[0].wind.speed * 3.6 * 10))/10 + "km/hr";

    // Set the wind degree
    windDeg.innerHTML = data.list[0].wind.deg + "<sup>o</sup>";

    const todaysDate = new Date(data.list[0].dt_txt);
    const currentMonth = monthsName[todaysDate.getMonth()];
    const currentDay = todaysDate.getDay();
    date.innerHTML = todaysDate.getDate() + " " + currentMonth;

    let j = 0;
    for(var i =0; i<days.length; ++i, j=j+8) {
        const dayName = daysName[(currentDay + i) % 7];
        days[i].innerHTML = dayName;

        const temp = Math.round((data.list[j].main.temp * 10))/10;
        temps[i].innerHTML = temp + "<sup>o</sup>";

        icons[i].src = imagePath + data.list[j].weather[0].icon + ".svg";
    }
}


cityForm.addEventListener('submit', e => {
    e.preventDefault();
    let cityName = cityForm.city.value.trim();
    if(cityName === "") {
        cityName = defaultCity;
    }
    
    getForecast(cityName)
        .then(data => updateUI(data, cityName))
        .catch(err => alert(err));
});
