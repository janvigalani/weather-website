const key = "3d01a80628e40c23a49e9cfc669385b8"
const getForecast = async(city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&appid=${key}`;

    const response = await fetch(base + query);

    if(response.ok) {
        const data = await response.json()
        return data;
    } else {
        throw new Error('Status Code: ' + response.status);
    }
}